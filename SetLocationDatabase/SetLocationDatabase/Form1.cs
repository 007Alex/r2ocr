﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using R2OCR;

namespace SetLocationDatabase
{
    public partial class Form1 : Form
    {
        //обработчик
        R2OCR.R2ScreenHandler SHandler = new R2ScreenHandler();
        
        public Form1()
        {
            InitializeComponent();
        }

        //проверка выделения
        private void CheckSelection()
        {
            //проверям что естьвыделенный элемент
            int index = listBox1.SelectedIndex;
            if (index != -1)
            {
                //смотрим выделенный элемент
                string curItem = listBox1.SelectedItem.ToString();

                //ищем его в другом листбоксе.
                index = listBox2.FindString(curItem);

                //если элемент отсутствует в списке
                if (index == -1)
                {
                    button1.Enabled = true;
                    label3.Text = "можно добавить";
                }
                else //если он уже есть
                {
                    listBox2.SetSelected(index, true);
                    button1.Enabled = false;
                    label3.Text = "уже есть";
                }
            }
            else //если он уже есть
            {
                button1.Enabled = false;
                label3.Text = "";
            }
        }

        //добавление элемента в список list1
        private void addStringToFirstList(string s)
        {
            if(s != "" && (listBox1.Items.Count == 0 || s != listBox1.Items[0].ToString()))
            {
                listBox1.Items.Insert(0, s);
                if (listBox1.Items.Count > 10)
                    listBox1.Items.RemoveAt(listBox1.Items.Count - 1);
            }
        }

        //Изменение выделения в листбоксе
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckSelection();
        }

        //Кнопка добавления
        private void button1_Click(object sender, EventArgs e)
        {
            int index = listBox1.SelectedIndex;
            if (index != -1)
            {
                listBox2.Items.Add(listBox1.SelectedItem.ToString());
                CheckSelection();
            }
        }

        //кнопка тестового добавления в лист 1
        private void button2_Click(object sender, EventArgs e)
        {
            addStringToFirstList(textBox1.Text);
        }

        //кнопка запуска/остановки таймера
        private void button3_Click(object sender, EventArgs e)
        {
            if(button3.Text == "Запуск")
            {
                button3.Text = "Стоп";
                timer1.Enabled = true;
            }
            else
            {
                button3.Text = "Запуск";
                timer1.Enabled = false;
            }
        }

        //таймер для сканирования
        private void timer1_Tick(object sender, EventArgs e)
        {
            //обрабатываем экран
            SHandler.ScanScreen();

            //вытаскиваем инфу
            addStringToFirstList(SHandler.GetPlacement().location);
        }
    }
}
