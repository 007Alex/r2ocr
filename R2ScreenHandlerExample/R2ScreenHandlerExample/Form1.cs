﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//подключаем
using R2OCR;

namespace R2ScreenHandlerExample
{
    public partial class Form1 : Form
    {
        //создаём обработчик
        R2ScreenHandler SHandler = new R2ScreenHandler();

        FormChat FChat = new FormChat();

        public Form1()
        {
            InitializeComponent();
            FChat.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled)
            {
                timer1.Enabled = false;
                FChat.Hide();
                button1.Text = "Старт";
                checkBox1.Enabled = true;
            }
            else
            {
                checkBox1.Enabled = false;
                //this.SendToBack();
                timer1.Enabled = true;
                FChat.Show();
                button1.Text = "Стоп";
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            SHandler.SetMode(checkBox1.Checked);
        }



        void NullLabels()
        {
            //время
            FChat.label1.Text = "-";
            FChat.label13.Text = "-";
            FChat.label19.Text = "-";
            FChat.label14.Text = "-";
            //значения
            FChat.label17.Text = "-";
            FChat.label18.Text = "-";
            FChat.label3.Text = "-";
            FChat.label4.Text = "-";
        }

        void CheckScreen()
        {
            System.Diagnostics.Stopwatch myStopwatch = new System.Diagnostics.Stopwatch();

            myStopwatch.Start(); //запуск

            //скармливаем скрин
            SHandler.ScanScreen();

            myStopwatch.Stop(); //остановить

            FChat.label1.Text = myStopwatch.ElapsedMilliseconds.ToString();

            FChat.label12.Text = SHandler.FrameIsCaptured().ToString();
        }


        string GetNickname()
        {
            System.Diagnostics.Stopwatch myStopwatch = new System.Diagnostics.Stopwatch();

            myStopwatch.Start(); //запуск

            string N = SHandler.GetNickname();

            myStopwatch.Stop(); //остановить

            FChat.label13.Text = myStopwatch.ElapsedMilliseconds.ToString();

            FChat.label18.Text = N;

            return N;
        }

        string GetGuild()
        {
            System.Diagnostics.Stopwatch myStopwatch = new System.Diagnostics.Stopwatch();

            myStopwatch.Start(); //запуск

            string G = SHandler.GetGuild();

            myStopwatch.Stop(); //остановить

            FChat.label19.Text = myStopwatch.ElapsedMilliseconds.ToString();

            FChat.label17.Text = G;

            return G;
        }

        void Placement()
        {
            System.Diagnostics.Stopwatch myStopwatch = new System.Diagnostics.Stopwatch();

            myStopwatch.Start(); //запуск

            R2ScreenHandler.Place res = SHandler.GetPlacement();

            myStopwatch.Stop(); //остановить

            FChat.label14.Text = myStopwatch.ElapsedMilliseconds.ToString();

            FChat.label3.Text = res.server;

            FChat.label4.Text = res.location;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            System.Diagnostics.Stopwatch myStopwatch = new System.Diagnostics.Stopwatch();

            myStopwatch.Start(); //запуск

            //обнуление
            NullLabels();

            //скрин
            CheckScreen();

            //тип
            FChat.label2.Text = SHandler.GetFrameType().ToString();

            if (SHandler.GetFrameType() == Frame.FrameType.human)
            {
                //ник 
                string N = GetNickname();

                //гильдия
                string G = GetGuild();

                string s = N + "    " + G;

                if ((FChat.listView1.Items.Count - 1 < 0 ||
                    s != FChat.listView1.Items[FChat.listView1.Items.Count - 1].Text)
                    && s != "    ")
                {
                    if (FChat.listView1.Items.Count > 15)
                        FChat.listView1.Items.RemoveAt(0);

                    FChat.listView1.Items.Add(s);

                    FChat.listView1.EnsureVisible(FChat.listView1.Items.Count - 1);
                }

                //сервер и локация
                Placement();
            }

            myStopwatch.Stop(); //остановить

            FChat.label21.Text = myStopwatch.ElapsedMilliseconds.ToString();          
        }
    }
}
