﻿using System;
using System.Drawing;


namespace R2OCR
{
    //Объект хранящий подробное описание символа
    [Serializable]
    public class Symbol
    {
        //типы символов
        public enum SymbolType : int
        {
            unknown, //для неизвестных (русскийх и английских одновременно)
            Eng, //для однозначно русских
            Rus, // для однозначно английских
            digit, //для цифр
            sign // для знаков (-+=?!;:._[]{}%^` и т.д.)
        }
        //цвет фона (не учитывается при поиске)
        public static Color backgroundColor = Color.FromArgb(255, 0, 0, 0);
        //изображение символа
        public Bitmap img = new Bitmap(1, 1);
        //стартовая позиция распознавания (та, точка, на которую мы бы наткнулись первой)
        public Point startPos = new Point(0, 0);
        //тип символа
        public SymbolType type = SymbolType.unknown;
        //символьное представление символа
        public char symbolChar = ' ';

        public Symbol()
        {
        }

        public Symbol(Bitmap IMG, Point StartPos, char SymbolChar, SymbolType Type)
        {
            if (img != null)
                img.Dispose();
            img = IMG;
            startPos = StartPos;
            type = Type;
            symbolChar = SymbolChar;
        }

        ~Symbol()
        {
            dispose();
        }

        /// <summary>
        ///Освобождение ресурсов
        /// </summary>
        public void dispose()
        {
            if (img != null)
                img.Dispose();
        }
    }
}
