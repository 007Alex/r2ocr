﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R2OCR
{
    public static class ConstsClasses
    {
        //константы рамки
        public static class FrameConsts
        {
            public static Size Size = new Size(205, 55);
            public static Size SizeSmall = new Size(205, 40);
            public static int Pos_dY0 = 176; //вверх, отностиельно высоты экрана
            public static int Pos_dX0 = 103; //влево, относительно центра 

            public static Point GetStartPositionFrame(Size screen_size)
            {
                return new Point(screen_size.Width / 2 - Pos_dX0,
                    screen_size.Height - Pos_dY0);
            }
        }
        //константы локации
        public static class LocationConsts
        {
            public static Size Size = new Size(370, 13);
            public static int Pos_dY0 = 85; //вверх, отностиельно высоты экрана
            public static int Pos_X0 = 19; //вправо (конкретная позиция)

            public static Point GetStartPositionLocantion(Size screen_size)
            {
                return new Point(Pos_X0,
                    screen_size.Height - Pos_dY0);
            }
        }
    }
}
