﻿using System.Drawing;

namespace R2OCR
{

    /// <summary>
    /// Обработчик экрана R2. Возвращает иформацию 
    /// о текущем захваченном объекте, сервере и локации.
    /// </summary>
    public class R2ScreenHandler
    {
        public struct Place
        {
            public string server;
            public string location;
        }

        //рамка
        private Frame fr;
        //распознавание
        private TextRecognition OCR;
        private string OCR_DataBase = "recognition.dat";

        //константы EPS
        private const int EPSforGuild = 72;
        private const int EPSforNick = 100;
        private const int EPSforOCR = 40;
        private const int EPSforTildas = 55;

        //текущий режим работы
        private bool FullscreenMode = true;

        //захват для распознавания рамки
        private bool FrameFound = false;

        //текущий скриншот
        private Bitmap ScreenShot = null;

        //захват для распознавания локации
        private bool LocationFound = false;

        //стартовая позиция для текста локации
        private Point LocationStartPoint = new Point(0, 0);

        //______________________________________________PRIVATE________________________________

        //поиск координат рамки при отключенном Fullscreen 
        private Point GetStartPositionFrame(Bitmap img)
        {           
            int minX = 0;
            int maxX = 0;
            int maxY = 0;
            int minY = 0;
            int X_inside = 0;
            int Y_inside = 0;
            //по всем пикселям изображения
            for(X_inside = 0; X_inside < img.Width; X_inside++)
            {
                bool flag = false;
                for(Y_inside = 0; Y_inside< img.Height; Y_inside++)
                {
                    //если пиксель рамки
                    if (ScreenSeeker.CheckColorEquality(img.GetPixel(X_inside, Y_inside),
                        Frame.frameColor, Frame.frameColorEPS))
                    {
                        flag = true;
                    }
                    //если не рамка
                    else
                    {
                        //если встречали рамку
                        if(flag)
                        {
                            flag = false;
                            //обнуляем данные
                            minX = 0;
                            maxX = 0;
                            maxY = 0;
                            minY = 0;
                            //поиск maxX
                            for (int i = X_inside; i < img.Width; i++)
                            {
                                if (ScreenSeeker.CheckColorEquality(img.GetPixel(i, Y_inside),
                                    Frame.frameColor, Frame.frameColorEPS))
                                {
                                    maxX = i;
                                    break;
                                }
                            }
                            //поиск minX
                            for (int i = X_inside; i >= 0; i--)
                            {
                                if (ScreenSeeker.CheckColorEquality(img.GetPixel(i, Y_inside),
                                    Frame.frameColor, Frame.frameColorEPS))
                                {
                                    minX = i;
                                    break;
                                }
                            }
                            //поиск maxY
                            for (int i = Y_inside; i < img.Height; i++)
                            {
                                if (ScreenSeeker.CheckColorEquality(img.GetPixel(X_inside, i),
                                    Frame.frameColor, Frame.frameColorEPS))
                                {
                                    maxY = i;
                                    break;
                                }
                            }
                            //поиск minY
                            for (int i = Y_inside; i >= 0; i--)
                            {
                                if (ScreenSeeker.CheckColorEquality(img.GetPixel(X_inside, i),
                                    Frame.frameColor, Frame.frameColorEPS))
                                {
                                    minY = i;
                                    break;
                                }
                            }

                            //смотрим результат
                            if (minX != 0 && minY != 0 && maxX != 0 && maxY != 0)
                            {
                                //если по размерам рамочка, то выходим с выводом результата
                                if(maxX - minX == ConstsClasses.FrameConsts.Size.Width &&
                                    maxY - minY == ConstsClasses.FrameConsts.Size.Height)
                                {
                                    return new Point(minX, minY);
                                }
                                if (maxX - minX == ConstsClasses.FrameConsts.SizeSmall.Width &&
                                    maxY - minY == ConstsClasses.FrameConsts.SizeSmall.Height)
                                {
                                    return new Point(minX, minY - 15);
                                }
                            }

                        }
                    }

                }
            }
            //если не вышли раньше, то ничего не найдено
            return new Point(0, 0);
        }

        //поиск координат текста локации
        private Point GetStartPositionLocation(Bitmap img, Point FramePosition)
        {
            if(FramePosition.X == 0 && FramePosition.Y == 0)
            {
                return FramePosition;
            }
            else
            {
                Point res = new Point(0, FramePosition.Y +
                    ConstsClasses.FrameConsts.Pos_dY0 - ConstsClasses.LocationConsts.Pos_dY0);
                //проходимся по горизонтали и ищем белые пиксели
                for(int i = img.Width/2; i > 0; i--)
                {
                    //если находим, то обновляем результат
                    if(ScreenSeeker.CheckColorEquality(
                        img.GetPixel(i,res.Y + ConstsClasses.LocationConsts.Size.Height/2),
                        Frame.White, 0))
                    {
                        res.X = i;
                    }
                }
                res.X -= 2;
                if (res.X > 0) return res;
                else
                {
                    res.X = 0;
                    res.Y = 0;
                    return res;
                }

            }
        }

        //разбирает строку, полученною после распознавания
        private Place GetPlaceFromOCRLine(string line)
        {
            Place res;
            res.location = "";
            res.server = "";

            bool write = false;
            bool location = false;
            for(int i = 0; i < line.Length; i++)
            {
                if (line[i] == '[') write = true;
                else
                {
                    if (line[i] == ']')
                    {
                        write = false;
                        location = true;
                    }
                    else
                    {
                        if (write)
                        {
                            if (location)
                                res.location += line[i];
                            else
                                res.server += line[i];
                        }
                    }
                }
                
            }
            return res;
        }

        //_____________________________________________PUBLIC__________________________________


        /// <summary>
        /// Создаёт объект обработчика экрана. 
        /// По умолчанию включен режим Fullscreen.
        /// </summary>
        public R2ScreenHandler(bool fullscreen_mode = true)
        {
            fr = new Frame();
            OCR = new TextRecognition(OCR_DataBase);
            FullscreenMode = fullscreen_mode;
        }

        /// <summary>
        /// Изменение режима работы обработчика. 
        /// При полноэкранном режиме экономится время на поиске местоположения рамки. 
        /// </summary>
        public void SetMode(bool fullscreen_mode)
        {
            FullscreenMode = fullscreen_mode;
        }

        /// <summary>
        /// Сканировать экран и проверить наличие на нём рамки.  
        /// </summary>
        public bool ScanScreen(Bitmap screenshot = null)
        {
            if (ScreenShot != null)
                ScreenShot.Dispose();

            if (screenshot == null)
                ScreenShot = ScreenSeeker.GetScreen();
            else
                ScreenShot = new Bitmap(screenshot);
            
            //если режим работы фулскрин
            if (FullscreenMode)
            {
                Point start = new Point(0, 0);
                start = ConstsClasses.FrameConsts.GetStartPositionFrame(ScreenShot.Size);
                //установим значения положения и размеров рамки на новые
                fr.SetPosition(start.X, start.X + ConstsClasses.FrameConsts.Size.Width,
                    start.Y, start.Y + ConstsClasses.FrameConsts.Size.Height);
                //попробуем извлеч рамку из скрина
                FrameFound = fr.GetFrameScreen(ScreenShot);    
            }
            //если режим работы не полноэкранный
            else
            {
                //ручное обнуление 
                fr.Capture = false;
                FrameFound = false;
                fr.Ftype = Frame.FrameType.unknown;

                //по всем пикселям изображения
                for (int X_inside = 0; X_inside < ScreenShot.Width; X_inside += ConstsClasses.FrameConsts.Size.Width - 2)
                {
                    for (int Y_inside = 0; Y_inside < ScreenShot.Height; Y_inside += ConstsClasses.FrameConsts.Size.Height - 2)
                    {
                        //поиск
                        FrameFound = fr.FindFrame(X_inside, Y_inside, ScreenShot);
                        
                        if (FrameFound)
                        {
                            //попробуем извлеч рамку из скрина
                            FrameFound = fr.GetFrameScreen(ScreenShot);
                            break;
                        }
                    }
                    if (FrameFound)
                        break;
                }
            }
            //если рамка найдена, то имеет смысл
            //сразу перевести её в чёрнобелый цвет
            if (FrameFound)
                fr.ConvertIntoBW(EPSforNick, EPSforGuild);
            else
            {
                //по всем пикселям изображения
                for (int X_inside = 0; X_inside < ScreenShot.Width; X_inside += ConstsClasses.FrameConsts.Size.Width - 2)
                {
                    for (int Y_inside = 0; Y_inside < ScreenShot.Height; Y_inside += ConstsClasses.FrameConsts.SizeSmall.Height - 2)
                    {
                        //поиск
                        FrameFound = fr.FindFrame(X_inside, Y_inside, ScreenShot);

                        if (FrameFound)
                        {
                            //попробуем извлеч рамку из скрина
                            FrameFound = fr.GetFrameScreen(ScreenShot);
                            break;
                        }
                    }
                    if (FrameFound)
                        break;
                }
            }
            //поиск локации
            if (FullscreenMode)
            {
                LocationStartPoint = ConstsClasses.LocationConsts.GetStartPositionLocantion(ScreenShot.Size);
                LocationFound = true;
            }
            else
            {       
                LocationStartPoint = GetStartPositionLocation(ScreenShot, new Point(fr.minX, fr.minY));
                if (LocationStartPoint.X != 0 && LocationStartPoint.Y != 0)
                    LocationFound = true;
                else
                    LocationFound = false;
            }

            return FrameFound;
        }

        /// <summary>
        /// Порверка на присутствие рамки в скрине  
        /// </summary>
        public bool FrameIsCaptured(){return FrameFound;}

        /// <summary>
        /// Порверка на присутствие локации в скрине  
        /// </summary>
        public bool LocationIsCaptured() { return LocationFound; }

        /// <summary>
        /// Порверка на присутствие гильдии в рамке 
        /// </summary>
        public bool GuildIsCaptured() { return fr.GuildFrame; }

        /// <summary>
        /// Получить тип рамки 
        /// </summary>
        public Frame.FrameType GetFrameType() { return fr.Ftype; }

        /// <summary>
        /// Получить имя персонажа
        /// </summary>
        public string GetNickname()
        {
            if (FrameFound)
                return fr.GetNickname(OCR, EPSforOCR, true, EPSforTildas);
            else
                return "";
        }

        /// <summary>
        /// Получить гильдию персонажа
        /// </summary>
        public string GetGuild()
        {
            //если есть рамки и в рамке есть гильдия
            if (FrameFound && fr.GuildFrame)
                return fr.GetGuild(OCR, EPSforOCR, true, EPSforTildas);
            else
                return "";
        }

        /// <summary>
        /// Получить строку c описанием локации и сервера
        /// </summary>
        public Place GetPlacement()
        {
            Place res;
            res.server = "";
            res.location = "";
            if (ScreenShot != null && LocationFound)
            {
                Bitmap img = ScreenShot.Clone(
                    new Rectangle(LocationStartPoint.X, LocationStartPoint.Y,
                    ConstsClasses.LocationConsts.Size.Width, ConstsClasses.LocationConsts.Size.Height),
                    ScreenShot.PixelFormat);

                string line = OCR.GetString(img, EPSforOCR, false, 25,
                    true, true);

                img.Dispose();

                res = GetPlaceFromOCRLine(line);

            }
            return res;
        }

        /// <summary>
        /// Получить скриншот который обрабатывался
        /// </summary>
        public Bitmap GetScreenShot() { return ScreenShot; }


        /// <summary>
        /// Освободить ресурсы
        /// </summary>
        public void dispose()
        {
            fr.dispose();
            OCR.dispose();
            if(ScreenShot != null)
                ScreenShot.Dispose();
        }

        ~R2ScreenHandler()
        {
            dispose();
        }
    }
}
