﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace R2OCR
{
    public static class ScreenSeeker
    {
        /// <summary>
        /// Проверка на примерное совпадение цветов
        /// </summary>
        public static bool CheckColorEquality(Color Color1, Color Color2, int EPS = 10, int mode = 0) 
        {
            //в обе стороны
            if (mode == 0)
            {
                if (Math.Abs(Color1.A - Color2.A) > EPS)
                    return false;
                if (Math.Abs(Color1.B - Color2.B) > EPS)
                    return false;
                if (Math.Abs(Color1.G - Color2.G) > EPS)
                    return false;
                if (Math.Abs(Color1.R - Color2.R) > EPS)
                    return false;
            }
            //второй меньше
            if(mode < 0)
            {
                if (Color1.A - Color2.A > EPS || Color1.A - Color2.A < 0)
                    return false;
                if (Color1.B - Color2.B > EPS || Color1.B - Color2.B < 0)
                    return false;
                if (Color1.G - Color2.G > EPS || Color1.G - Color2.G < 0)
                    return false;
                if (Color1.R - Color2.R > EPS || Color1.R - Color2.R < 0)
                    return false;
            }
            //второй больше
            if(mode > 0)
            {
                if (Color2.A - Color1.A > EPS || Color2.A - Color1.A < 0)
                    return false;
                if (Color2.B - Color1.B > EPS || Color2.B - Color1.B < 0)
                    return false;
                if (Color2.G - Color1.G > EPS || Color2.G - Color1.G < 0)
                    return false;
                if (Color2.R - Color1.R > EPS || Color2.R - Color1.R < 0)
                    return false;
            }
            return true;
                
        }

        /// <summary>
        /// Считает разницу цветов по R, G и B оригинала и тени. Применяет эту разницу к новому цвету
        /// </summary>
        public static Color NewColorShadow(Color OrigColor, Color ShadowColor, Color NewOrigColor)
        {
            int EPS = OrigColor.R - ShadowColor.R;
            if (OrigColor.G - ShadowColor.G > EPS)
                EPS = OrigColor.G - ShadowColor.G;
            if (OrigColor.B - ShadowColor.B > EPS)
                EPS = OrigColor.B - ShadowColor.B;
            //EPS /= 3;
            int R = NewOrigColor.R - EPS;
            int G = NewOrigColor.G - EPS;
            int B = NewOrigColor.B - EPS;
            if (R < 0) R = 0;
            if (G < 0) G = 0;
            if (B < 0) B = 0;
            if (R > 255) R = 255;
            if (G > 255) G = 255;
            if (B > 255) B = 255;
            return Color.FromArgb(255, R, G, B);
        }

        /// <summary>
        /// Инвертирование цветов (255 - color)
        /// </summary>
        public static Color InvertColor(Color color)
        {
            return Color.FromArgb(255, 255 - color.R, 255 - color.G, 255 - color.B);
        }
        /// <summary>
        ///протсто возврашает скриншот
        /// </summary>
        public static Bitmap GetScreen() 
        {
            Bitmap result = new Bitmap(Screen.PrimaryScreen.Bounds.Size.Width, Screen.PrimaryScreen.Bounds.Size.Height, PixelFormat.Format24bppRgb);
            try
            {
                using (Graphics g = Graphics.FromImage(result))
                {
                    g.CopyFromScreen(0, 0, 0, 0, Screen.PrimaryScreen.Bounds.Size, CopyPixelOperation.SourceCopy);
                }
            }
            catch
            {
                result = null;
            }
            return result;
        }

        /// <summary>
        ///возвращает скриншот области
        /// </summary>
        public static Bitmap GetScreenPart(int x, int y, int width, int height) 
        {

            Bitmap result = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            Size s = new Size(width, height);
            try
            {
                using (Graphics g = Graphics.FromImage(result))
                {
                    g.CopyFromScreen(x,y, 0, 0, s, CopyPixelOperation.SourceCopy);
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }

        /// <summary>
        /// Масштабирование картинки до заданного размера.
        /// </summary>
        /// <param name="source"> Исходное изображение. </param>
        /// <param name="width"> Ширина целевого изображения. </param>
        /// <param name="height"> Высота целевого изображения. </param>
        /// <returns> Масштабированное изображение. </returns>
        public static Image ScaleImage(Image source, int width, int height)
        {
            Image dest = new Bitmap(width, height);
            using (Graphics gr = Graphics.FromImage(dest))
            {
                gr.FillRectangle(Brushes.White, 0, 0, width, height);  // Очищаем экран
                gr.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

                float srcwidth = source.Width;
                float srcheight = source.Height;
                float dstwidth = width;
                float dstheight = height;

                if (srcwidth <= dstwidth && srcheight <= dstheight)  // Исходное изображение меньше целевого
                {
                    int left = (width - source.Width) / 2;
                    int top = (height - source.Height) / 2;
                    gr.DrawImage(source, left, top, source.Width, source.Height);
                }
                else if (srcwidth / srcheight > dstwidth / dstheight)  // Пропорции исходного изображения более широкие
                {
                    float cy = srcheight / srcwidth * dstwidth;
                    float top = ((float)dstheight - cy) / 2.0f;
                    if (top < 1.0f) top = 0;
                    gr.DrawImage(source, 0, top, dstwidth, cy);
                }
                else  // Пропорции исходного изображения более узкие
                {
                    float cx = srcwidth / srcheight * dstheight;
                    float left = ((float)dstwidth - cx) / 2.0f;
                    if (left < 1.0f) left = 0;
                    gr.DrawImage(source, left, 0, cx, dstheight);
                }

                return dest;
            }
        }

        /// <summary>
        /// (Визуализация) Отрисовка пиксельной сетки, с увеличением размеров пикселей (масштабирование).
        /// </summary>
        public static Bitmap DrawGrid(Bitmap img, Color gridColor, int pixelSize = 2, int gridWidth = 1)
        {
            Bitmap result = new Bitmap(img.Width * (pixelSize + gridWidth) - 1, img.Height * (pixelSize + gridWidth) - 1);
            
            for(int i = 0; i < result.Width; i++)
            {
                for(int j = 0; j < result.Height; j++)
                {
                    if((i + 1) % (pixelSize + gridWidth) == 0 || (j + 1) % (pixelSize + gridWidth) == 0)
                    {
                        result.SetPixel(i, j, gridColor);
                    }
                    else
                    {
                        if (ScreenSeeker.CheckColorEquality(
                            img.GetPixel(i / (pixelSize + gridWidth), j / (pixelSize + gridWidth)),
                            Color.FromArgb(255, 255, 255, 255), 0))
                        {
                            result.SetPixel(i, j, Color.FromArgb(255, 0, 255, 0));
                        }
                        else
                        {
                            result.SetPixel(i, j,
                                img.GetPixel(i / (pixelSize + gridWidth), j / (pixelSize + gridWidth)));
                        }
                    }
                }
            }

            return result;
        }
    }
}
