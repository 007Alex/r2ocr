﻿using R2Seeker.Pages;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using EventHook;
using FirstFloor.ModernUI.Windows.Controls;
using EventHook.Hooks;
using System.Windows.Controls;
using System.Media;

namespace R2Seeker
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    /// 

    public partial class App : Application
    {
        // [STAThread]
        bool she = false;
        KeyboardListener KListener = new KeyboardListener();
        ModernDialog dlg = new ModernDialog { };
        public void aa()
        {
            dlg = new ModernDialog { Topmost = true };
            dlg.ShowDialog();  
        }
        public void Application_Startup(object sender, StartupEventArgs e)
        {
            MouseWatcher.Start();
            
        MouseWatcher.OnMouseInput += (s, f) =>
            {
                    if (f.Message == MouseMessages.WM_LBUTTONDOWN)
                    {
                        she = true;
                    }
                    if(f.Message == MouseMessages.WM_RBUTTONDOWN && she == true)
                    {
                    SystemSounds.Exclamation.Play();
                  //  dlg.Dispatcher.BeginInvoke(new Action(delegate ()
                  //  {
                   //     this.aa();
                   // }));
                }
                    if (f.Message == MouseMessages.WM_LBUTTONUP || f.Message == MouseMessages.WM_RBUTTONUP)
                    {
                        she = false;
                    }
                //MessageBox.Show(string.Format("Mouse event {0} at point {1},{2}", f.Message.ToString(), f.Point.x, f.Point.y));
            };
            KListener.KeyDown += new RawKeyEventHandler(KListener_KeyDown);
           // MouseWatcher.Stop();
        }

        void KListener_KeyDown(object sender, RawKeyEventArgs args)
        {
            //MessageBox.Show(args.Key.ToString());
            
        }
        void KListener_KeyDown2(object sender, RawKeyEventArgs args)
        {
            
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            KListener.Dispose();
        }
    }
}
