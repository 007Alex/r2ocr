﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace R2Seeker.Pages
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : UserControl
    {
        public Home()
        {
            InitializeComponent();
        }
        private void ScrollViewer_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void pop_Loaded(object sender, RoutedEventArgs e)
        {
                this.button_Click(null, null);
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Text1.Text = Hook.str;
        }
    }
}
