﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R2Seeker_Experimental.ScreenProcessing
{
    //Объект хранящий подробное описание символа
    [Serializable]
    public class Symbol
    {
        //типы символов
        public enum SymbolType : int
        {
            unknown, //для неизвестных (русскийх и английских одновременно)
            Eng, //для однозначно русских
            Rus, // для однозначно английских
            digit, //для цифр
            sign // для знаков (-+=?!;:._[]{}%^` и т.д.)
        }
        //цвет фона (не учитывается при поиске)
        public static Color backgroundColor = Color.FromArgb(255, 0, 0, 0);
        //изображение символа
        public Bitmap img;
        //стартовая позиция распознавания (та, точка, на которую мы бы наткнулись первой)
        public Point startPos;
        //тип символа
        public SymbolType type;
        //символьное представление символа
        public char symbolChar;

        public Symbol()
        {
            img = new Bitmap(0, 0);
            startPos = new Point(0, 0);
            type = SymbolType.unknown;
            symbolChar = ' ';
        }

        public Symbol(Bitmap IMG, Point StartPos, char SymbolChar, SymbolType Type)
        {
            img = IMG;
            startPos = StartPos;
            type = Type;
            symbolChar = SymbolChar;
        }
    }
}
