﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace R2Seeker_Experimental.ScreenProcessing
{

    /// <summary>
    ///Распознавание текста на изображении
    /// </summary>
    public class TextRecognition
    {
        public List<Symbol> SymbolDataBase;

        public static Color MaxSymbolColor = Color.FromArgb(255, 255, 255, 255);

        public TextRecognition()
        {
            SymbolDataBase = new List<Symbol>();
        }

        public TextRecognition(string filename)
        {
            SymbolDataBase = new List<Symbol>();
            try
            {
                LoadSymbolDataBase(filename);
            }
            catch
            {
                //...
            }
        }

        public void AddToSymbolDataBase(Symbol symbol)
        {
            SymbolDataBase.Add(symbol);
        }

        private static char[] UnknownRUS = {'А','а','В','Е','е','К','М','Н','О','о','Р','р','С','с','Т','у','Х','х'};
        private static char[] UnknownENG = {'A','a','B','E','e','K','M','H','O','o','P','p','C','c','T','y','X','x'};
        private void ConvertLanguage(ref string s, Symbol.SymbolType stype)
        {
            StringBuilder res = new StringBuilder(s);
            for(int i = 0; i < s.Length;i++)
            {
                for(int j = 0; j < UnknownENG.Length; j++)
                {
                    if(stype == Symbol.SymbolType.Eng || stype == Symbol.SymbolType.unknown)
                    {
                        if (s[i] == UnknownRUS[j])
                            res[i] = UnknownENG[j];
                    }
                    if(stype == Symbol.SymbolType.Rus)
                    {
                        if (s[i] == UnknownENG[j])
                            res[i] = UnknownRUS[j];
                    }
                }
            }
            s = res.ToString();
        }

        /// <summary>
        /// Сохранить базу в файл
        /// </summary>
        public void SaveSymbolDataBase(string filename, bool xml = false)
        {
            if (!xml)
                using (Stream stream = File.Open(filename, FileMode.Create))
                {
                    BinaryFormatter bin = new BinaryFormatter();
                    bin.Serialize(stream, SymbolDataBase);
                }
            else
                using (Stream stream = File.Open(filename, FileMode.Create))
                {

                }
        }
        /// <summary>
        /// Загрузить базу из файла
        /// </summary>
        public void LoadSymbolDataBase(string filename)
        {
            using (Stream stream = File.Open(filename, FileMode.Open))
            {
                BinaryFormatter bin = new BinaryFormatter();
                SymbolDataBase = (List<Symbol>)bin.Deserialize(stream);
            }
        }

        /// <summary>
        /// Поиск символа из базы по первому вхождению на изображении
        /// </summary>
        public Symbol FindSymbol(ref Bitmap img, Point StartPos, int MaxColorSumEPS = 10)
        {
            //int minEPS = MaxColorSumEPS;
            int maxPixels = 0;
            int maxInd = -1;
            int maxAllPixels = -1;
            //по всем символам
            for(int k = 0; k < SymbolDataBase.Count; k++)
            {
                //угловая точка для подстановки текущего символа
                Point p = new Point(StartPos.X - SymbolDataBase[k].startPos.X,
                    StartPos.Y - SymbolDataBase[k].startPos.Y);
                if (p.X < 0 || p.Y < 0) continue;
                //подставляем символ в изображение и считаем пиксели картинок
                int pixels = 0;
                int allPixels = 0;
                bool flagExit = false;
                //проход по размерам изображения символа
                for(int i = 0; i < SymbolDataBase[k].img.Width; i++)
                {
                    for (int j = 0; j < SymbolDataBase[k].img.Height; j++)
                    {
                        //проверка на цвет пикселя символа, а не её фона
                        if(!ScreenSeeker.CheckColorEquality(SymbolDataBase[k].img.GetPixel(i,j),
                            Symbol.backgroundColor))
                        {
                            allPixels++;
                            //если цвет пикселя идеально белый
                            if (ScreenSeeker.CheckColorEquality(SymbolDataBase[k].img.GetPixel(i, j),
                                MaxSymbolColor, 0))
                            {
                                //если цвета совпадают
                                if (p.X + i < img.Width && p.Y + j < img.Height &&
                                    ScreenSeeker.CheckColorEquality(SymbolDataBase[k].img.GetPixel(i, j),
                                    img.GetPixel(p.X + i, p.Y + j), 0))
                                {
                                    pixels++;
                                }
                                //если цвета не совпадает, то это явно не тот символ
                                else
                                {
                                    pixels = 0;
                                    flagExit = true;
                                    break;
                                }
                            }
                            //если цвет пикселя буквы не идельно белый
                            else
                            {
                                //если сопостовимый пиксель на изображении не чёрный
                                if (p.X + i < img.Width && p.Y + j < img.Height &&
                                    !ScreenSeeker.CheckColorEquality(Color.FromArgb(255,255,255,255),
                                    img.GetPixel(p.X + i, p.Y + j), 0))
                                {
                                    //если цвета схожи
                                    if(ScreenSeeker.CheckColorEquality(SymbolDataBase[k].img.GetPixel(i, j),
                                    img.GetPixel(p.X + i, p.Y + j), MaxColorSumEPS))
                                        pixels++;

                                }
                                else
                                {
                                    pixels = 0;
                                    flagExit = true;
                                    break;
                                }
                            }
                        }
                        //в противном случае на изображении 
                        //точно не должно быть пикселя максимального цвета
                        else
                        {
                            //если цвет на изображении не белый
                            if (p.X + i < img.Width && p.Y + j < img.Height &&
                                !ScreenSeeker.CheckColorEquality(MaxSymbolColor,
                                img.GetPixel(p.X + i, p.Y + j), 0))
                            {

                            }
                            //если фон чёрный а цвет на изображении максимально белый
                            else
                            {
                                pixels = 0;
                                flagExit = true;
                                break;
                            }
                        }
                    }
                    //если нет смысла продолжать
                    if (flagExit) break;
                }
                //улучшение итогового результата
                if(!flagExit && (pixels > maxPixels ||
                    allPixels < maxAllPixels && pixels == maxPixels))
                {
                    maxPixels = pixels;
                    maxAllPixels = allPixels;
                    maxInd = k;
                }
            }
            //вывод подходящего символа
            if (maxInd == -1)
                return null;
            else
            {
                //угловая точка для подстановки текущего символа
                Point p = new Point(StartPos.X - SymbolDataBase[maxInd].startPos.X,
                    StartPos.Y - SymbolDataBase[maxInd].startPos.Y);
                //закрашиваем область символа
                for (int i = 0; i < SymbolDataBase[maxInd].img.Width; i++)
                {
                    for (int j = 0; j < SymbolDataBase[maxInd].img.Height; j++)
                    {
                        img.SetPixel(p.X + i, p.Y + j, Symbol.backgroundColor);
                    }
                }
                return SymbolDataBase[maxInd];
            }
        }

        /// <summary>
        /// Распознавание строки текста путём сравнения с базой символов
        /// </summary>
        public string GetString(Bitmap img, int MaxColorSumEPS = 10, bool TildasExisting = false, int MaxTildasEPS = 25)
        {
            string result = "";
            Symbol.SymbolType curtype = Symbol.SymbolType.unknown;
            List<Point> CharPosition = new List<Point>();
            //поиск и сравнение с базой
            for (int i = 0; i < img.Width;i++ )
            {
                for(int j = 0; j < img.Height; j++)
                {
                    //при встрече чисто белого цвета
                    if(ScreenSeeker.CheckColorEquality(img.GetPixel(i,j), MaxSymbolColor, 0))
                    {
                        Symbol symbol = FindSymbol(ref img, new Point(i, j), MaxColorSumEPS);
                        if (symbol != null)
                        {                           
                            if (symbol.type == Symbol.SymbolType.Rus) curtype = symbol.type;
                            if (symbol.type == Symbol.SymbolType.Eng) curtype = symbol.type;
                            result += symbol.symbolChar;
                            CharPosition.Add(new Point(i, j));
                        }
                    }
                }
            }
            //наличие тильды <`>
            if(TildasExisting)
            {
                //проходимся по верхней половине строки
                for(int i = 0; i < img.Width; i++)
                {
                    for(int j = 0; j < img.Height; j++)
                    {
                        //если встречаем возможный символ
                        if (ScreenSeeker.CheckColorEquality(img.GetPixel(i, j),
                            MaxSymbolColor, MaxTildasEPS))
                        {
                            Point minRes = new Point(i,j);
                            Point maxRes = new Point(i, j);
                            CharBasicSegmentation(ref img, minRes, ref maxRes,
                                ref minRes, MaxSymbolColor, MaxTildasEPS);
                            //добавить тильду в результат
                            bool pushed = false;
                            for(int k = 0; k < CharPosition.Count; k++)
                            {
                                if(CharPosition[k].X > minRes.X)
                                {
                                    pushed = true;
                                    CharPosition.Insert(k, minRes);
                                    result = result.Insert(k, "`");
                                    break;
                                }
                            }
                            if(!pushed)
                            {
                                CharPosition.Add(minRes);
                                result += "`";
                            }
                            //делаем скачёк на размер этого символа
                            i = maxRes.X;
                            break;
                        }
                    }
                }
            }
            ConvertLanguage(ref result, curtype);
            return result;
        }


        /// <summary>
        ///поиск границ символа и возврат его изображения 
        /// </summary>
        private static Bitmap CharBFS(ref Bitmap img,Point p, ref Point maxp, ref Point minp, Color B, Color G, Color W)
        {
            Bitmap empty_img = new Bitmap(img); 
            minp = p;
            maxp = p;
            Point free;
            Color freeCol;
            Queue<Point> q = new Queue<Point>();
            Queue<Color> qCol = new Queue<Color>();
            q.Enqueue(p);
            qCol.Enqueue(img.GetPixel(p.X, p.Y));
            //копируем пиксель в отдельное изображение
            empty_img.SetPixel(p.X, p.Y, img.GetPixel(p.X, p.Y));
            //закрашиваем пиксель после обработки в оригинале
            img.SetPixel(p.X, p.Y, W);

            //объод в ширину
            while (q.Count != 0)
            {
                //вытаскиваем из очереди
                free = q.Dequeue();
                freeCol = qCol.Dequeue();
                
                //все 8 направлений
                for (int i = -1; i < 2; i++)
                {
                    for(int j = -1; j < 2; j++)
                    {
                        //если не та же клетка, от которой мы идём
                        if (i != 0 || j != 0)
                        {
                            //если * -> B (из любого в первичный признак)
                            //или B -> G (из первичного во вторичный)
                            if (ScreenSeeker.CheckColorEquality(img.GetPixel(free.X + i, free.Y + j), B) ||
                                ScreenSeeker.CheckColorEquality(img.GetPixel(free.X + i, free.Y + j), G) &&
                                ScreenSeeker.CheckColorEquality(freeCol, B))
                            {
                                free.X += i;
                                free.Y += j;
                                q.Enqueue(free);
                                qCol.Enqueue(img.GetPixel(free.X, free.Y));
                                //копируем пиксель в отдельное изображение
                                //empty_img.SetPixel(free.X, free.Y, img.GetPixel(free.X, free.Y));
                                //закрашиваем пиксель в оригинале
                                img.SetPixel(free.X, free.Y, W);
                                free.X -= i;
                                free.Y -= j;
                            }
                        }
                    }
                }    
                //изменение minp и maxp
                if (minp.X > free.X) minp.X = free.X;
                if (minp.Y > free.Y) minp.Y = free.Y;
                if (maxp.X < free.X) maxp.X = free.X;
                if (maxp.Y < free.Y) maxp.Y = free.Y;
            }
            //возвращаем кусок изображения с символом
            return empty_img.Clone(new Rectangle(minp.X, minp.Y, maxp.X - minp.X + 1, maxp.Y - minp.Y + 1), empty_img.PixelFormat);
        }


        /// <summary>
        ///поиск границ символа и возврат его изображения 
        /// </summary>
        private static Bitmap CharBasicSegmentation(ref Bitmap img, Point p, ref Point maxp, ref Point minp, Color B, int colorEPS)
        {
            minp = p;
            maxp = p;
            bool flag = true;
            Point free;
            free = p;
            //
            while (flag)
            {
                flag = false;
                //по высоте
                for (free.Y = 0; free.Y < img.Height; free.Y++)
                {
                    if (ScreenSeeker.CheckColorEquality(img.GetPixel(free.X, free.Y), B, colorEPS))
                    {
                        if (minp.Y > free.Y) minp.Y = free.Y;
                        if (maxp.Y < free.Y) maxp.Y = free.Y;
                        flag = true;
                    }
                }
                if (flag && maxp.X < free.X) maxp.X = free.X;
                free.X++;
                if (free.X >= img.Width)
                    break;
            }
            return img.Clone(new Rectangle(minp.X, minp.Y, maxp.X - minp.X + 1, maxp.Y - minp.Y + 1), img.PixelFormat);
        }


        /// <summary>
        /// выделение символов по цвету
        /// (B - первичный цвет, G - вторичный цвет(тень), W - цвет фона)
        /// Добавление изображений и координат в соответсвующие листы 
        /// (MinPosPoint - содержит minX minY позиции символа отностильено изображения)
        /// </summary>
        public static void GetCharBitmaps(Bitmap img, ref List<Bitmap> CharBitmapsList, ref List<Point> MinPosPoint,Color B, Color G, Color W, bool BFSmode = true, int colorEPS = 40)
        {
            //копия изображения, для изменений
            Bitmap copy_img = new Bitmap(img);

            Point p = new Point(0,0);
            Point minp = new Point();
            Point maxp = new Point();
            //проход по всем пикселям
            for (p.X = 0; p.X < img.Width; p.X++)
            {
                for (p.Y = 0; p.Y < img.Height; p.Y++)
                {
                    //при встрече первичного признака символа, запускаем BFS
                    if (ScreenSeeker.CheckColorEquality(copy_img.GetPixel(p.X, p.Y), B, colorEPS))
                    {
                        //сохраняем результата в листы
                        if (BFSmode)
                        {
                            CharBitmapsList.Add(CharBFS(ref copy_img, p, ref maxp, ref minp, B, G, W));
                            MinPosPoint.Add(minp);
                        }
                        else
                        {
                            CharBitmapsList.Add(CharBasicSegmentation(ref copy_img, p, ref maxp, ref minp, B, colorEPS));
                            MinPosPoint.Add(minp);
                            p.X = maxp.X;
                            break;
                        }
                    }
                }
            }
        }
    }
}
