﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace R2Seeker_Experimental.ScreenProcessing
{
    /// <summary>
    /// Объект рамки с ником
    /// </summary>
    public class Frame
    {
        //типы рамок
        public enum FrameType
        {
            unknown,
            mob,
            human,
            npc
        }
        //задаём цвет искомой рамки
        public static Color frameColor = Color.FromArgb(255,194,175,124);
        public static int frameColorEPS = 10;
        //информация о положении рамки
        public bool Capture;
        public int minX;
        public int maxX;
        public int minY;
        public int maxY;
        //последнее изображение рамки
        public Bitmap FrameIMG;
        //тип последнего изображения рамки
        public FrameType Ftype = FrameType.unknown;
        //наличие гильдии
        public bool GuildFrame = true;

        //цвета текста в рамке
        static Color NickColor = Color.FromArgb(255, 255, 255, 255);
        static Color GuildColor = Color.FromArgb(255, 102, 204, 153);
        //основной порог цвета
        const int nickEPS = 40;
        const int guildEPS = 30;
        //(старое)цвета заполнения пикселей по признакам
        public static Color Black = Color.FromArgb(255, 0, 0, 0);
        public static Color Gray = Color.FromArgb(255, 128, 128, 128);
        public static Color White = Color.FromArgb(255, 255, 255, 255);

        //отношение занимаемого пространства для знака гильдии
        //(чтобы затерать эту часть)
        public static double guildSingPart = 6;

        /// <summary>
        /// создание объекта новой рамки
        /// </summary>
        public Frame()
        {
            Capture = false;
            minX = 0;
            maxX = 0;
            maxY = 0;
            minY = 0;
        }

        /// <summary>
        ///создание объекта известной рамки
        /// </summary>
        public Frame(int x_min, int x_max,int y_min, int y_max)
        {
            Capture = true;
            minX = x_min;
            maxX = x_max;
            maxY = y_max;
            minY = y_min;
        }

        /// <summary>
        ///Установить позицию рамки
        /// </summary>
        public void SetPosition(int x_min, int x_max, int y_min, int y_max)
        {
            Capture = true;
            minX = x_min;
            maxX = x_max;
            maxY = y_max;
            minY = y_min;
        }

        /// <summary>
        /// поиск рамки через координаты внутренней точки (для новой рамки)
        /// </summary>
        public bool FindFrame(int X_inside, int Y_inside, Bitmap scr = null)
        {
            //обнуляем данные
            Capture = false;
            minX = 0;
            maxX = 0;
            maxY = 0;
            minY = 0;

            if(scr == null)
                scr = ScreenSeeker.GetScreen();

            //поиск maxX
            for (int i = X_inside; i < scr.Width; i++)
            {
                if (ScreenSeeker.CheckColorEquality(scr.GetPixel(i,Y_inside), frameColor, frameColorEPS))
                {
                    maxX = i;
                    break;
                }
            }
            //поиск minX
            for (int i = X_inside; i >= 0; i--)
            {
                if (ScreenSeeker.CheckColorEquality(scr.GetPixel(i, Y_inside), frameColor, frameColorEPS))
                {
                    minX = i;
                    break;
                }
            }
            //поиск maxY
            for (int i = Y_inside; i < scr.Height; i++)
            {
                if (ScreenSeeker.CheckColorEquality(scr.GetPixel(X_inside, i), frameColor, frameColorEPS))
                {
                    maxY = i;
                    break;
                }
            }
            //поиск minY
            for (int i = Y_inside; i >= 0; i--)
            {
                if (ScreenSeeker.CheckColorEquality(scr.GetPixel(X_inside, i), frameColor, frameColorEPS))
                {
                    minY= i;
                    break;
                }
            }

            //смотрим результат
            if (minX == 0 || minY == 0 || maxX == 0 || maxY == 0)
            {
                return false;
            }
            else
            {
                Capture = true;
                return true;
            }
        }

        /// <summary>
        /// получение изображения в области рамки (сохраняется в FrameIMG)
        /// </summary>
        public bool GetFrameScreen(Bitmap screenshot = null)
        {
            if(screenshot == null)
                FrameIMG = ScreenSeeker.GetScreenPart(minX, minY, maxX - minX, maxY - minY);
            else
                FrameIMG = screenshot.Clone(
                new Rectangle(minX, minY, maxX - minX, maxY - minY),
                FrameIMG.PixelFormat);

            GetType();

            return ScreenSeeker.CheckColorEquality(
                FrameIMG.GetPixel(0, FrameIMG.Height / 2),
                frameColor);
        }

        private void GetType()
        {
            //если ничего из нижеречисленного, то неизвестно 
            //(возможно не прогрузилась гильдия)
            GuildFrame = false;
            Ftype = FrameType.unknown;

            //смотрим верхнюю часть рамки для определения типа
            for (int i = (int)(FrameIMG.Width / guildSingPart); i < FrameIMG.Width - 4; i++)
            {
                for (int j = 2; j < FrameIMG.Height / 2 - 1; j++)
                {
                    //если есть чёрный, то это моб
                    if (ScreenSeeker.CheckColorEquality(FrameIMG.GetPixel(i, j), Black))
                    {
                        Ftype = FrameType.mob;
                        GuildFrame = false;
                        //return;
                    }
                    //если есть цвет рамки, то это человек без гильдии
                    if (ScreenSeeker.CheckColorEquality(FrameIMG.GetPixel(i, j), frameColor))
                    {
                        if (Ftype != FrameType.mob)
                        {
                            Ftype = FrameType.human;
                            GuildFrame = false;
                        }
                        //return;
                    }
                    //если есть цвет названия гильдии, то это человек с гильдией
                    if (ScreenSeeker.CheckColorEquality(FrameIMG.GetPixel(i, j), GuildColor, 0))
                    {
                        Ftype = FrameType.human;
                        GuildFrame = true;
                        //return;
                    }
                    //если есть цвет никнейма, то это скорее всего NPC
                    if (ScreenSeeker.CheckColorEquality(FrameIMG.GetPixel(i, j), NickColor, 0))
                    {
                        Ftype = FrameType.npc;
                        GuildFrame = false;
                        //return;
                    }
                }
            }

        }

        /// <summary>
        ///Обработать полученное изображение в чёрнобелое, где: 
        ///чёрное/серое - пиксель буквы, белое - другое. (изменения происходят в FrameIMG)
        /// </summary>
        public void ConvertIntoBW(int nEPS = nickEPS, int gEPS = guildEPS, bool fillGuildSign = true)
        {
            //проход по всем пикселям
            for (int i = 0; i < FrameIMG.Height; i++)
            {
                for (int j = 0; j < FrameIMG.Width; j++)
                {
                    //если в зоне с знаком гилдьии
                    if(FrameIMG.Width / guildSingPart > j && fillGuildSign)
                    {
                        //закрашиваем
                        FrameIMG.SetPixel(j, i, Black);
                    }
                    else //если в обычной зоне
                    {
                        //если это часть ника
                        if (ScreenSeeker.CheckColorEquality(FrameIMG.GetPixel(j, i), NickColor, nEPS))
                        {   
                            //ничего не делать (сохраним цвета пикселей)
                        }
                        //если часть названия гильдии
                        else if (ScreenSeeker.CheckColorEquality(FrameIMG.GetPixel(j, i), GuildColor, gEPS, 1))
                        {
                            FrameIMG.SetPixel(j, i,
                                ScreenSeeker.NewColorShadow(GuildColor, 
                                FrameIMG.GetPixel(j, i), NickColor));
                        }
                        // в противном случае закрашивыаем чёрным
                        else
                        { 
                            FrameIMG.SetPixel(j, i, Black);
                        }
                        /*
                        if(!ScreenSeeker.CheckColorEquality(FrameIMG.GetPixel(j,i), White, 0) &&
                            !ScreenSeeker.CheckColorEquality(FrameIMG.GetPixel(j, i), Black, 0))
                        {
                            FrameIMG.SetPixel(j, i, Gray);
                        }*/
                    }
                }
            }
        }

        /// <summary>
        /// распознать ник
        /// </summary>
        public string GetNickname(ref TextRecognition OCR, int MaxColorSumEPS = 10, bool TildasExisting = false, int MaxTildasEPS = 25)
        {

            //нижняя часть изображения
            Bitmap img = FrameIMG.Clone(new Rectangle(0, FrameIMG.Height / 2, FrameIMG.Width - 2,
                FrameIMG.Height / 2 - 2), FrameIMG.PixelFormat);

            //распознавание
            return OCR.GetString(img, MaxColorSumEPS, TildasExisting, MaxTildasEPS);
        }


        /// <summary>
        /// распознать Гильдию
        /// </summary>
        public string GetGuild(ref TextRecognition OCR, int MaxColorSumEPS = 10, bool TildasExisting = false, int MaxTildasEPS = 25)
        {
            if (GuildFrame)
            {
                //верхняя часть изображения
                Bitmap img = FrameIMG.Clone(new Rectangle(0, 0, FrameIMG.Width - 2,
                    FrameIMG.Height / 2), FrameIMG.PixelFormat);

                //распознавание
                return OCR.GetString(img, MaxColorSumEPS, TildasExisting, MaxTildasEPS);
            }
            else
                return "";
        }


        /// <summary>
        /// (Для демонстрации) Выделяет все символы в нике и добавляет информацию в соответсвующие листы
        /// </summary>
        public void GetNickCharBitmaps(ref List<Bitmap> CharBitmapsList, ref List<Point> MinPosPoint, bool BFSmode = true, int colorEPS = 100)
        {
            //нижняя часть изображения
            Bitmap img = FrameIMG.Clone(new Rectangle(0, FrameIMG.Height / 2 - 1, FrameIMG.Width - 2, FrameIMG.Height / 2), FrameIMG.PixelFormat);
            //обаботка 
            TextRecognition.GetCharBitmaps(img, ref CharBitmapsList, ref MinPosPoint, White, Gray, Black, BFSmode, colorEPS);
        }

        /// <summary>
        /// (Для демонстрации) Выделяет все символы в нике и добавляет информацию в соответсвующие листы
        /// </summary>
        public void GetGuildCharBitmaps(ref List<Bitmap> CharBitmapsList, ref List<Point> MinPosPoint, bool BFSmode = true, int colorEPS = 100)
        {
            //нижняя часть изображения
            Bitmap img = FrameIMG.Clone(new Rectangle(0, 0, FrameIMG.Width - 2, FrameIMG.Height / 2), FrameIMG.PixelFormat);
            //обаботка 
            TextRecognition.GetCharBitmaps(img, ref CharBitmapsList, ref MinPosPoint, White, Gray, Black, BFSmode, colorEPS);
        }


        /// <summary>
        /// (визуализация) нарисовать красную линию центра (сохраняется в FrameIMG)
        /// </summary>
        public void DrawRedCenterLine()
        {
            for (int j = 0; j < FrameIMG.Width; j++)
            {
                FrameIMG.SetPixel(j, FrameIMG.Height / 2, Color.FromArgb(255,255,0,0));
            }
        }
    }
}
