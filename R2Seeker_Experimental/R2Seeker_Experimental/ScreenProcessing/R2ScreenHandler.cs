﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R2Seeker_Experimental.ScreenProcessing
{
    /// <summary>
    /// Обработчик экрана R2. Возвращает иформацию 
    /// о текущем захваченном объекте, сервере и локации.
    /// </summary>
    class R2ScreenHandler
    {
        //рамка
        private Frame fr;
        //распознавание
        private TextRecognition OCR;
        private string OCR_DataBase = "recognition.dat";

        //константы для положения объектов FullScreen
        //константы рамки
        private static class FrameConsts
        {
            public static Size Size = new Size(205, 55);
            public static Size SizeSmall = new Size(205, 40);
            public static int Pos_dY0 = 176; //вверх, отностиельно высоты экрана
            public static int Pos_dX0 = 103; //влево, относительно центра 

            public static Point GetStartPositionFrame(Size screen_size)
            {
                return new Point(screen_size.Width/2 - Pos_dX0,
                    screen_size.Height - Pos_dY0);
            }
        }
        //константы рамки
        private static class LocationConsts
        {
            public static Size Size = new Size(260, 13);
            public static int Pos_dY0 = 85; //вверх, отностиельно высоты экрана
            public static int Pos_X0 = 19; //вправо (конкретная позиция)

            public static Point GetStartPositionLocantion(Size screen_size)
            {
                return new Point(Pos_X0,
                    screen_size.Height - Pos_dY0);
            }
        } 

        //константы EPS
        private const int EPSforGuild = 72;
        private const int EPSforNick = 100;
        private const int EPSforOCR = 40;
        private const int EPSforTildas = 55;

        //текущий режим работы
        private bool FullscreenMode = true;

        //захват для распознавания рамки
        private bool FrameFound = false;

        //текущий скриншот
        private Bitmap ScreenShot = null;

        //захват для распознавания локации
        private bool LocationFound = false;

        //стартовая позиция для текста локации
        private Point LocationStartPoint = new Point(0, 0);

        //______________________________________________PRIVATE________________________________

        //поиск координат рамки при отключенном Fullscreen 
        private Point GetStartPositionFrame(ref Bitmap img)
        {           
            int minX = 0;
            int maxX = 0;
            int maxY = 0;
            int minY = 0;
            int X_inside = 0;
            int Y_inside = 0;
            //по всем пикселям изображения
            for(X_inside = 0; X_inside < img.Width; X_inside++)
            {
                bool flag = false;
                for(Y_inside = 0; Y_inside< img.Height; Y_inside++)
                {
                    //если пиксель рамки
                    if (ScreenSeeker.CheckColorEquality(img.GetPixel(X_inside, Y_inside),
                        Frame.frameColor, Frame.frameColorEPS))
                    {
                        flag = true;
                    }
                    //если не рамка
                    else
                    {
                        //если встречали рамку
                        if(flag)
                        {
                            flag = false;
                            //обнуляем данные
                            minX = 0;
                            maxX = 0;
                            maxY = 0;
                            minY = 0;
                            //поиск maxX
                            for (int i = X_inside; i < img.Width; i++)
                            {
                                if (ScreenSeeker.CheckColorEquality(img.GetPixel(i, Y_inside),
                                    Frame.frameColor, Frame.frameColorEPS))
                                {
                                    maxX = i;
                                    break;
                                }
                            }
                            //поиск minX
                            for (int i = X_inside; i >= 0; i--)
                            {
                                if (ScreenSeeker.CheckColorEquality(img.GetPixel(i, Y_inside),
                                    Frame.frameColor, Frame.frameColorEPS))
                                {
                                    minX = i;
                                    break;
                                }
                            }
                            //поиск maxY
                            for (int i = Y_inside; i < img.Height; i++)
                            {
                                if (ScreenSeeker.CheckColorEquality(img.GetPixel(X_inside, i),
                                    Frame.frameColor, Frame.frameColorEPS))
                                {
                                    maxY = i;
                                    break;
                                }
                            }
                            //поиск minY
                            for (int i = Y_inside; i >= 0; i--)
                            {
                                if (ScreenSeeker.CheckColorEquality(img.GetPixel(X_inside, i),
                                    Frame.frameColor, Frame.frameColorEPS))
                                {
                                    minY = i;
                                    break;
                                }
                            }

                            //смотрим результат
                            if (minX != 0 && minY != 0 && maxX != 0 && maxY != 0)
                            {
                                //если по размерам рамочка, то выходим с выводом результата
                                if(maxX - minX == FrameConsts.Size.Width &&
                                    maxY - minY == FrameConsts.Size.Height)
                                {
                                    return new Point(minX, minY);
                                }
                                if (maxX - minX == FrameConsts.SizeSmall.Width &&
                                    maxY - minY == FrameConsts.SizeSmall.Height)
                                {
                                    return new Point(minX, minY - 15);
                                }
                            }

                        }
                    }

                }
            }
            //если не вышли раньше, то ничего не найдено
            return new Point(0, 0);
        }

        //поиск координат текста локации
        private Point GetStartPositionLocation(ref Bitmap img, Point FramePosition)
        {
            if(FramePosition.X == 0 && FramePosition.Y == 0)
            {
                return FramePosition;
            }
            else
            {
                Point res = new Point(0, FramePosition.Y + 
                    FrameConsts.Pos_dY0 - LocationConsts.Pos_dY0);
                //проходимся по горизонтали и ищем белые пиксели
                for(int i = img.Width/2; i > 0; i--)
                {
                    //если находим, то обновляем результат
                    if(ScreenSeeker.CheckColorEquality(
                        img.GetPixel(i,res.Y + LocationConsts.Size.Height/2),
                        Frame.White, 0))
                    {
                        res.X = i;
                    }
                }
                res.X -= 2;
                if (res.X > 0) return res;
                else
                {
                    res.X = 0;
                    res.Y = 0;
                    return res;
                }

            }
        }

        //_____________________________________________PUBLIC__________________________________


        /// <summary>
        /// Создаёт объект обработчика экрана. 
        /// По умолчанию включен режим Fullscreen.
        /// </summary>
        public R2ScreenHandler(bool fullscreen_mode = true)
        {
            fr = new Frame();
            OCR = new TextRecognition(OCR_DataBase);
            FullscreenMode = fullscreen_mode;
        }

        /// <summary>
        /// Изменение режима работы обработчика. 
        /// При полноэкранном режиме экономится время на поиске местоположения рамки. 
        /// </summary>
        public void ChangeMode(bool fullscreen_mode)
        {
            FullscreenMode = fullscreen_mode;
        }

        /// <summary>
        /// Сканировать экран и проверить наличие на нём рамки.  
        /// </summary>
        public bool ScanScreen(Bitmap screenshot = null)
        {
            if (screenshot == null)
                ScreenShot = ScreenSeeker.GetScreen();
            else
                ScreenShot = screenshot;

            Point start = new Point(0, 0);
            //если режим работы фулскрин
            if (FullscreenMode)
            {
                start = FrameConsts.GetStartPositionFrame(ScreenShot.Size);
                //установим значения положения и размеров рамки на новые
                fr.SetPosition(start.X, start.X + FrameConsts.Size.Width,
                    start.Y, start.Y + FrameConsts.Size.Height);
                //попробуем извлеч рамку из скрина
                FrameFound = fr.GetFrameScreen(ScreenShot);    
            }
            //если режим работы не полноэкранный
            else
            {
                start = GetStartPositionFrame(ref ScreenShot);
                if (start.X == 0 && start.Y == 0) FrameFound = false;
                else
                {
                    //установим значения положения и размеров рамки на новые
                    fr.SetPosition(start.X, start.X + FrameConsts.Size.Width,
                        start.Y, start.Y + FrameConsts.Size.Height);
                    //попробуем извлеч рамку из скрина
                    FrameFound = fr.GetFrameScreen(ScreenShot);
                }
            }
            //если рамка найдена, то имеет смысл
            //сразу перевести её в чёрнобелый цвет
            if (FrameFound)
                fr.ConvertIntoBW(EPSforNick, EPSforGuild);

            //поиск локации
            LocationStartPoint = GetStartPositionLocation(ref ScreenShot, start);
            if (LocationStartPoint.X != 0 && LocationStartPoint.Y != 0)
                LocationFound = true;
            else
                LocationFound = false;

            return FrameFound;
        }

        /// <summary>
        /// Порверка на присутствие рамки в скрине  
        /// </summary>
        public bool FrameIsCaptured(){return FrameFound;}

        /// <summary>
        /// Порверка на присутствие локации в скрине  
        /// </summary>
        public bool LocationIsCaptured() { return LocationFound; }

        /// <summary>
        /// Получить тип рамки 
        /// </summary>
        public Frame.FrameType GetFrameType() { return fr.Ftype; }

        /// <summary>
        /// Получить имя персонажа
        /// </summary>
        public string GetNickname()
        {
            if (FrameFound)
                return fr.GetNickname(ref OCR, EPSforOCR, true, EPSforTildas);
            else
                return "";
        }

        /// <summary>
        /// Получить гильдию персонажа
        /// </summary>
        public string GetGuild()
        {
            //если есть рамки и в рамке есть гильдия
            if (FrameFound && fr.GuildFrame)
                return fr.GetNickname(ref OCR, EPSforOCR, true, EPSforTildas);
            else
                return "";
        }

        /// <summary>
        /// Получить строку c описанием локации
        /// </summary>
        public string GetLocation()
        {
            if (ScreenShot != null && LocationFound)
            {
                //нижняя часть изображения
                Bitmap img = ScreenShot.Clone(
                    new Rectangle(LocationStartPoint.X, LocationStartPoint.Y,
                    LocationConsts.Size.Width, LocationConsts.Size.Height),
                    ScreenShot.PixelFormat);
                return OCR.GetString(img, EPSforOCR);
            }
            else
                return "";

        }

        /// <summary>
        /// Получить скриншот который обрабатывался
        /// </summary>
        public Bitmap GetScreenShot() { return ScreenShot; }
    }
}
