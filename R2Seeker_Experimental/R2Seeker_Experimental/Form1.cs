﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using R2Seeker_Experimental.ScreenProcessing;

namespace R2Seeker_Experimental
{
    public partial class Form1 : Form
    {
        //рамка
        Frame fr;
        //распознавание
        TextRecognition OCR;
        //имя файла
        string OCR_DataBase = "recognition.dat";

        public Form1()
        {
            InitializeComponent();
            fr = new Frame();
            OCR = new TextRecognition(OCR_DataBase);
        }

        //_____________________________вкладка 1_________________________________________________________

        private void button1_Click(object sender, EventArgs e)
        {
            if (fr.FindFrame(MousePosition.X, MousePosition.Y))
            {
                fr.GetFrameScreen();
                //fr.ConvertIntoBW();
                fr.DrawRedCenterLine();
                pictureBox1.Image = fr.FrameIMG;
                label3.Text = (fr.maxX - fr.minX).ToString();
                label4.Text = (fr.maxY - fr.minY).ToString();
                label40.Text = fr.minX.ToString();
                label41.Text = fr.minY.ToString();
            }
            else
            {
                label3.Text = "-";
                label4.Text = "-";
                label40.Text = "-";
                label41.Text = "-";
            }
            pictureBox2.Image = ScreenSeeker.GetScreen();
        }

        //_____________________________вкладка 2_________________________________________________________

        private void button2_Click(object sender, EventArgs e)
        {
            if (fr.Capture)
            {
                label46.Text = fr.GetFrameScreen().ToString();
                fr.ConvertIntoBW(trackBar2.Value,trackBar1.Value);  
                textBox1.Text = fr.GetGuild(ref OCR, (int)numericUpDown1.Value, true, 55) 
                    + "\r\n" + fr.GetNickname(ref OCR, (int)numericUpDown1.Value, true, 55);
                fr.DrawRedCenterLine();
                pictureBox3.Image = fr.FrameIMG;
                label44.Text = fr.Ftype.ToString();
                label45.Text = fr.GuildFrame.ToString();
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            label1.Text = trackBar1.Value.ToString();
            if (fr.Capture)
            {
                fr.GetFrameScreen();
                fr.ConvertIntoBW(trackBar2.Value, trackBar1.Value);
                fr.DrawRedCenterLine();
                pictureBox3.Image = fr.FrameIMG;
            }
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            label2.Text = trackBar2.Value.ToString();
            if (fr.Capture)
            {
                fr.GetFrameScreen();
                fr.ConvertIntoBW(trackBar2.Value, trackBar1.Value);
                fr.DrawRedCenterLine();
                pictureBox3.Image = fr.FrameIMG;
            }
        }

        //_____________________________вкладка 3________________________________________________________

        //для демонстрации выделения символов 
        List<Bitmap> ChBitmaps = new List<Bitmap>();
        List<Point> ChMinPos = new List<Point>();
        int CurImgIndex = 0;

        //для чата (ручной подбор)
        int ChatColorEPS = 102;
        Rectangle MyChatPosition = new Rectangle(76, 973, 244, 17);

        //функция обновления для демонстрации выделения символов
        private void CharBitmapsUpdate()
        {
            label11.Text = ChBitmaps.Count.ToString();
            if (ChBitmaps.Count != 0)
            {
                pictureBoxForCharacters.Image = ScreenSeeker.DrawGrid(ChBitmaps[CurImgIndex], Color.FromArgb(255, 255, 0, 0), 4);
                label12.Text = (CurImgIndex + 1).ToString();
                label13.Text = ChMinPos[CurImgIndex].X.ToString();
                label14.Text = ChMinPos[CurImgIndex].Y.ToString();
                label15.Text = ChBitmaps[CurImgIndex].Width.ToString();
                label16.Text = ChBitmaps[CurImgIndex].Height.ToString();
                //поиск точки вхождения (максимального цвета символа, магисмально белого цвета)
                for(int i = 0; i < ChBitmaps[CurImgIndex].Width; i++)
                {
                    for(int j = 0; j < ChBitmaps[CurImgIndex].Height; j++)
                    {
                        if( ScreenSeeker.CheckColorEquality(ChBitmaps[CurImgIndex].GetPixel(i,j),
                            TextRecognition.MaxSymbolColor, 0))
                        {
                            textBox4.Text = i.ToString();
                            textBox3.Text = j.ToString();
                            return;
                        }
                    }
                }

            }
            else
            {
                pictureBoxForCharacters.Image = null;
                label12.Text = "-";
                label13.Text = "-";
                label14.Text = "-";
                label15.Text = "-";
                label16.Text = "-";
            }
        }

        private void buttonSegmentation_Click(object sender, EventArgs e)
        {
            if (fr.Capture)
            {
                fr.GetFrameScreen();
                fr.ConvertIntoBW(trackBar2.Value, trackBar1.Value);

                ChBitmaps.Clear();
                ChMinPos.Clear();
                CurImgIndex = 0;

                //pictureBox4.Image = fr.FrameIMG;

                fr.GetNickCharBitmaps(ref ChBitmaps, ref ChMinPos, false, trackBar2.Value);

                CharBitmapsUpdate();

                fr.DrawRedCenterLine();
                pictureBox4.Image = fr.FrameIMG;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (ChBitmaps.Count != 0)
            {
                CurImgIndex++;
                if (CurImgIndex >= ChBitmaps.Count)
                    CurImgIndex = 0;
                CharBitmapsUpdate();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (ChBitmaps.Count != 0)
            {
                CurImgIndex--;
                if (CurImgIndex < 0)
                    CurImgIndex = ChBitmaps.Count - 1;
                CharBitmapsUpdate();
            }
        }

        private void buttonChatScan_Click(object sender, EventArgs e)
        {
            fr.FrameIMG = ScreenSeeker.GetScreenPart(MyChatPosition.X, MyChatPosition.Y,
                MyChatPosition.Width, MyChatPosition.Height);
            fr.ConvertIntoBW(ChatColorEPS, trackBar1.Value, false);

            ChBitmaps.Clear();
            ChMinPos.Clear();
            CurImgIndex = 0;

            TextRecognition.GetCharBitmaps(fr.FrameIMG, ref ChBitmaps, ref ChMinPos,
                Color.FromArgb(255, 255, 255, 255), Color.FromArgb(255, 128, 128, 128),
                Color.FromArgb(255, 0, 0, 0), false, ChatColorEPS);

            CharBitmapsUpdate();

            pictureBox4.Image = fr.FrameIMG;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Symbol.SymbolType type = Symbol.SymbolType.unknown;

            if (comboBox1.SelectedIndex == 1) type = Symbol.SymbolType.Eng;
            if (comboBox1.SelectedIndex == 2) type = Symbol.SymbolType.Rus;
            if (comboBox1.SelectedIndex == 3) type = Symbol.SymbolType.digit;
            if (comboBox1.SelectedIndex == 4) type = Symbol.SymbolType.sign;

            Symbol a = new Symbol(ChBitmaps[CurImgIndex],
                new Point(Convert.ToInt32(textBox4.Text), Convert.ToInt32(textBox3.Text)),
                textBox2.Text[0], type);
            OCR.AddToSymbolDataBase(a);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (fr.Capture)
            {
                fr.GetFrameScreen();
                fr.ConvertIntoBW(trackBar2.Value, trackBar1.Value);

                ChBitmaps.Clear();
                ChMinPos.Clear();
                CurImgIndex = 0;

                //pictureBox4.Image = fr.FrameIMG;

                fr.GetGuildCharBitmaps(ref ChBitmaps, ref ChMinPos, false, trackBar2.Value);

                CharBitmapsUpdate();

                fr.DrawRedCenterLine();
                pictureBox4.Image = fr.FrameIMG;
            }
        }

        //_____________________________вкладка 4_______________________________________________________

        //текущий индекс символа в базе для демонстрации
        int OCR_CurImgIndex = 0;
        //обновление демонстрации базы символов
        private void SDatabsePictureBoxUpdate()
        {
            label29.Text = OCR.SymbolDataBase.Count.ToString();
            if (OCR.SymbolDataBase.Count != 0)
            {
                pictureBox5.Image = ScreenSeeker.DrawGrid(
                    OCR.SymbolDataBase[OCR_CurImgIndex].img, Color.FromArgb(255, 255, 0, 0), 4);
                label30.Text = (OCR_CurImgIndex + 1).ToString();
                label31.Text = OCR.SymbolDataBase[OCR_CurImgIndex].img.Width.ToString();
                label32.Text = OCR.SymbolDataBase[OCR_CurImgIndex].img.Height.ToString();
                label34.Text = OCR.SymbolDataBase[OCR_CurImgIndex].startPos.X.ToString();
                label35.Text = OCR.SymbolDataBase[OCR_CurImgIndex].startPos.Y.ToString();
                label33.Text = OCR.SymbolDataBase[OCR_CurImgIndex].symbolChar.ToString();
                label37.Text = OCR.SymbolDataBase[OCR_CurImgIndex].type.ToString();
            }
            else
            {
                pictureBox5.Image = null;
                label30.Text = "-";
                label31.Text = "-";
                label32.Text = "-";
                label34.Text = "-";
                label35.Text = "-";
                label33.Text = "-";
                label37.Text = "-";
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            OCR_CurImgIndex++;
            if (OCR_CurImgIndex >= OCR.SymbolDataBase.Count)
                OCR_CurImgIndex = 0;
            SDatabsePictureBoxUpdate();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            OCR_CurImgIndex--;
            if (OCR_CurImgIndex < 0)
                OCR_CurImgIndex = OCR.SymbolDataBase.Count - 1;
            SDatabsePictureBoxUpdate();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            OCR.SymbolDataBase.RemoveAt(OCR_CurImgIndex);
            button6_Click(sender, e);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            OCR.SaveSymbolDataBase(OCR_DataBase);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (textBox5.Text.Length != 0)
            {
                for (int i = 0; i < OCR.SymbolDataBase.Count; i++)
                {
                    if (OCR.SymbolDataBase[i].symbolChar == textBox5.Text[0])
                    {
                        OCR_CurImgIndex = i;
                        SDatabsePictureBoxUpdate();
                        break;
                    }
                }
            }
        }

        //перевод в базу для dll
        private void button12_Click(object sender, EventArgs e)
        {
            R2OCR.TextRecognition newTR = new R2OCR.TextRecognition();
            for(int i = 0; i < OCR.SymbolDataBase.Count;i++)
            {
                R2OCR.Symbol.SymbolType nType = R2OCR.Symbol.SymbolType.unknown;

                if(OCR.SymbolDataBase[i].type == Symbol.SymbolType.sign)
                    nType = R2OCR.Symbol.SymbolType.sign;
                if (OCR.SymbolDataBase[i].type == Symbol.SymbolType.Rus)
                    nType = R2OCR.Symbol.SymbolType.Rus;
                if (OCR.SymbolDataBase[i].type == Symbol.SymbolType.Eng)
                    nType = R2OCR.Symbol.SymbolType.Eng;
                if (OCR.SymbolDataBase[i].type == Symbol.SymbolType.digit)
                    nType = R2OCR.Symbol.SymbolType.digit;

                newTR.SymbolDataBase.Add(new R2OCR.Symbol(
                    OCR.SymbolDataBase[i].img,
                    OCR.SymbolDataBase[i].startPos,
                    OCR.SymbolDataBase[i].symbolChar,
                    nType));
            }
            newTR.SaveSymbolDataBase("newRecognititon.dat");
            newTR.SymbolDataBase.Clear();
            newTR.LoadSymbolDataBase("newRecognititon.dat");
        }
    }
}
